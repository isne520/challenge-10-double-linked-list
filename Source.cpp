#include <iostream>
#include "list.h"
using namespace std;
void main()
{

	List bas;//create list name is bas
	bas.headPush(4);
	bas.headPush(5);
	bas.tailPush(8);  //add elements in list
	bas.tailPush(3);
	bas.tailPush(4);
	bas.tailPop();
	//delete head and tail
	bas.headPop();

	if (bas.isInList(4)) {// ask 4 is in list? 
		cout << "Is in list" << endl;//yes
	}
	else {
		cout << "Isn't in list" << endl;//no
	}
	system("pause");
}