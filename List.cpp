#include <iostream>
#include "list.h"

using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int list)//Add element to front of list
{
	Node *tmp = new Node(list); //create new node name is tmp

	if (isEmpty()) //not have any element in list 
	{
		tail = tmp; // tail point to tmp
		head = tmp; // head point to tmp

	}
	else //list is not empty
	{
		tmp->next = head;//tmp point to head
		head->prev = tmp;
		head = tmp; //now we have new head
	}

}

void List::tailPush(int list)//Add element to tail of list
{
	Node *tmp = new Node(list); //create new node name is tmp
	if (isEmpty())//not have any element in list
	{
		head = tmp; // head point to tmp
		tail = tmp; // tail point to tmp
	}
	else //list is not empty
	{
		tail->next = tmp;//tail point to next element is tmp
		tmp->prev = tail;
		tail = tmp;// now tail point to tmp
	}
}

int List::headPop()//Remove and return element from front of list
{
	int c; // create c to collect element
	Node *tmp = head; //create node name is tmp point to head
	if (isEmpty())//not have any element in list
	{
		return NULL;//not have any element to delete
	}
	else if (head == tail)//has just one element in list
	{

		c = tmp->info;//c point to head
		head = NULL; // point new head to nothing
		tail = NULL; // point tail to nothing 
		delete tmp;
		return c;
	}
	else//having more than one element in list
	{
		head = head->next; //point head to next element
		c = tmp->info;//c point to element in head
		delete tmp;
		head->prev = NULL;
		return c;
	}

}

int List::tailPop()//Remove and return element from tail of list
{
	int c;
	Node *tmp = tail;
	if (isEmpty())//not have any element in list
	{
		return NULL;//not have any element to delete
	}
	else if (tail == head)//has just one element in list
	{
		c = tmp->info;
		tail = NULL;
		head = NULL;
		delete tmp;
		return c;
	}
	else
	{
		tail = tail->prev;//move tail to previous list
		c = tmp->info;//c point to element in tmp
		delete tmp;
		tail->next = NULL;
		return c;

	}

}

void List::deleteNode(int list)//Delete a particular value
{
	Node *tmp = head;
	if (isEmpty())//not have any element in list
	{
		return;//nothing to delete
	}
	while (head->info == list)//head list equal to data that want to delete
	{

		head = head->next;//move head to next list
		delete tmp;
		head->prev = NULL;//head point to NULL

	}
	tmp = head->next;//tmp move next to head

	Node *p, *q;
	while (tmp != tail)//check tmp isn't tail
	{
		if (tmp->info == list) //tmp list is equal to data that want to delete 
		{

			p = tmp->prev;//p point to previous list of tmp
			q = tmp;//q that want to delete
			tmp = tmp->next;//move tmp 

							//delete
			delete q;

			//conection
			p->next = tmp;
			tmp->prev = p;
		}
		else// not have data to delete
		{
			tmp = tmp->next;//move
		}
	}
	if (tail->info == list)//check tmp is eqaul to data that want to delete
	{
		tail->prev = tmp;//move tmp backward
		q = tmp->next;//q move to tail
		tmp->next = NULL;//tmp point to NULL
		tail = tmp;//set new tail
		delete q;//delete
	}
	else
	{
		return;
	}
}
bool List::isInList(int list) //Check if a particular value is in the list
{
	Node *tmp = head;
	if (isEmpty())//list is empty
	{
		return false;//NOPE, I've not found
	}
	while (tmp != tail)//continue checking untill find tail 
	{
		if (tmp->info != list)//have not found data in list yet
		{
			tmp = tmp->next;//moving
		}
		else
		{
			return true;// yes! I've found it
		}
	}
	if (tmp->info == list) //tail is eqaul to data
		return true;//yes! I've found it
	else return false;//NOPE, I've not found
}
void List::display()//Function display
{
	Node *tmp = head;

	while (tmp != tail)//
	{
		cout << tmp->info;//show all data in list  
		tmp = tmp->next;
	}
	if (tmp != NULL) cout << tmp->info;//show data in tail

	return;
}